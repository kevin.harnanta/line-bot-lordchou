package main

import (
	"log"
	"os"
	"strings"
	"github.com/gin-gonic/gin"
	"github.com/line/line-bot-sdk-go/linebot"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/guaychou/go-tiktok-api"

)

var (
	totalRequestLine = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "total_line_bot_request",
		Help: "Current total of line bot request",
	},
		[]string{"code", "uri"},
	)
	messageSent = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "total_line_bot_message_sent",
		Help: "Current total of line bot message sent",
	})
	messageVideoSent = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "total_line_bot_video_sent",
		Help: "Current total of line bot message sent",
	})
)

func init() {
	gin.SetMode(gin.ReleaseMode)
	prometheus.MustRegister(totalRequestLine)
	prometheus.MustRegister(messageSent)
	prometheus.MustRegister(messageVideoSent)
}

func main() {
	//spawn gin framework
	r := gin.Default()
	channelSecret := os.Getenv("BOT_SECRET")
	channelToken := os.Getenv("BOT_TOKEN")
	bot, err := linebot.New(channelSecret, channelToken)
	if err != nil {
		log.Fatal(err)
	}
	// intialise tiktok
	t := tiktok.NewTiktok()

	// registering router to gin
	r.GET("/_/healthz", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"Status":  200,
			"message": "I'm Healthy",
		})
	})
	r.GET("/metrics", func(c *gin.Context) {
		h := promhttp.Handler()
		h.ServeHTTP(c.Writer, c.Request)
	})

	r.POST("/callback", func(c *gin.Context) {
		events, err := bot.ParseRequest(c.Request)
		if err != nil {
			if err == linebot.ErrInvalidSignature {
				totalRequestLine.WithLabelValues("400", c.Request.RequestURI).Inc()
				c.JSON(400, gin.H{
					"Status":  400,
					"message": linebot.ErrInvalidSignature,
				})

			} else {
				totalRequestLine.WithLabelValues("500", c.Request.RequestURI).Inc()
				c.JSON(500, gin.H{
					"Status":  500,
					"message": err,
				})
			}
			return
		}

		for _, event := range events {
			totalRequestLine.WithLabelValues("200", c.Request.RequestURI).Inc()
			if event.Type == linebot.EventTypeMessage {
				message := &linebot.TextMessage{}
				messageVideo := &linebot.VideoMessage{}
				switch messageFrom := event.Message.(type) {

				case *linebot.TextMessage:
					if strings.HasPrefix(messageFrom.Text, "/tiktok") {
						split := strings.Split(messageFrom.Text, " ")
						if len(split) != 2 {
							message = linebot.NewTextMessage("Some argument is missing. \nUse /tiktok <link> to download the video.")
						} else if len(split) == 2 {
							copiedLink := split[1]
							data,err:=t.GetVideo(copiedLink)
							if err!=nil{
								message = linebot.NewTextMessage(err.Error())
							}else {
								messageVideo = linebot.NewVideoMessage(data.VideoURL, data.ImageURL)
							}
						}
					} else if strings.Trim(messageFrom.Text, " ") == "/bye" {
						if event.Source.GroupID != "" {
							if _, err = bot.LeaveGroup(event.Source.GroupID).Do(); err != nil {
								log.Println(err)
							}
						} else if event.Source.RoomID != "" {
							if _, err = bot.LeaveRoom(event.Source.RoomID).Do(); err != nil {
								log.Println(err)
							}
						}
					} else if strings.HasPrefix(messageFrom.Text, "/help") {
						message = linebot.NewTextMessage("Usage of the bot: /tiktok <url> to share tiktok videos")
					}else if strings.HasPrefix(messageFrom.Text, "/start") {
						message = linebot.NewTextMessage("Hello i am a babu of lordchou")
					}
				}
				if messageVideo.OriginalContentURL != "" && messageVideo.PreviewImageURL != "" {
					if _, err = bot.ReplyMessage(event.ReplyToken, messageVideo).Do(); err != nil {
						log.Print(err)
					}
					messageVideoSent.Inc()
				} else if message.Text != "" {
					if _, err = bot.ReplyMessage(event.ReplyToken, message).Do(); err != nil {
						log.Print(err)
					}
					messageSent.Inc()
				}
			}
		}
	})
	r.Run()
}
