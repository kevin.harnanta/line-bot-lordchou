module gitlab.com/kevin.harnanta/line-bot-lordchou

go 1.14

require (
	github.com/andybalholm/cascadia v1.2.0 // indirect
	github.com/antchfx/xpath v1.1.10 // indirect
	github.com/gin-gonic/gin v1.6.3
	github.com/go-playground/validator/v10 v10.3.0 // indirect
	github.com/guaychou/go-tiktok-api v0.0.3
	github.com/line/line-bot-sdk-go v7.4.0+incompatible
	github.com/prometheus/client_golang v1.7.1
	golang.org/x/sys v0.0.0-20200722175500-76b94024e4b6 // indirect
	golang.org/x/text v0.3.3 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
