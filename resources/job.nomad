job "[[ .job.name ]]" {
    type="[[.job.type]]"
    datacenters = ["[[.datacenter]]"]
    update {
        max_parallel      = "[[.update.max_parallel]]"
        health_check      = "[[.update.health_check]]"
        min_healthy_time  = "[[.update.min_healthy_time]]"
        healthy_deadline  = "[[.update.healthy_deadline]]"
        progress_deadline = "[[.update.progress_deadline]]"
        auto_revert       = "[[.update.auto_revert]]"
        stagger           = "[[.update.stagger]]"
    }

    group "[[.job.name]]" {
    task "[[.job.name]]" {
        service {
            name = "[[ .job.name ]]"
            tags =  [ [[ range $i:=.tags ]]
                    "[[$i]]",[[end]]
            ]
            port = "[[.application.port]]"            
            check {
                type = "[[.healthcheck.type]]"
                path = "[[.healthcheck.path]]"
                interval = "[[.healthcheck.interval]]"
                timeout = "[[.healthcheck.timeout]]"
                address_mode = "driver"    
            } 
            address_mode = "driver"    
        }

        driver = "docker"
        config {
            image = "[[ env "DOCKER_USERNAME" ]]/[[.application.image]]:[[.application.tag]]"
            force_pull = [[if .application.pullPolicy]][["true"]][[end]]
            dns_servers = ["172.17.0.1"]
        }
        env {
            BOT_TOKEN="[[ env "BOT_TOKEN" ]]"
            BOT_SECRET="[[ env "BOT_SECRET" ]]"
            PORT="[[.application.port]]"
        }
        resources {
            cpu    = "[[.resources.cpu]]"
            memory = "[[.resources.memory]]"
        }

    }   
}
}
